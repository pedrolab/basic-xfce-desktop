[TOC]

configure a debian bullseye xfce desktop for basic users

**warning**: right now, this tries to fit a very specific use case/project, so it's not generalized for everyone. open a new ticket if you want to discuss other uses

status: experimental

## about this project

This project was born as an experiment to provide good user experience for newbie users on GNU/Linux. On 2022, we prepared some laptops with a debian stable (11) with a preseed file

### the approach (about accessibility vs security)

This autoprovisioning system is designed to be a Debian GNU/Linux that is familiar for a windows user. That means prioritizing accessibility and user experience over security. Hence, here we have:

- autologin to a default user
- passwordless sudo (admin rights)
- passwordless gnome-keyring password store

Security must apply when the user is concerned and ready to go a step further

## Install this

Install a debian 11 stable. Alternatively [this guide](https://gitlab.com/dsg-upc/basic-xfce-desktop/-/tree/main/docs/install-debian-automated), also distributed in this repo, shows you step by step how to install debian automatically using a [preseed](https://wiki.debian.org/DebianInstaller/Preseed) for xfce.

### apply the script

Given a debian 11 stable installed with the xfce desktop, apply the script to configure it

#### the most easy way

Download and execute the following desktop application. On google chrome, just click the downloaded file

https://dsg.ac.upc.edu/config.desktop

#### download script with curl

```
curl -LOs https://dsg.ac.upc.edu/config ; sudo sh -e config.sh
```

#### download script with wget

```
wget https://dsg.ac.upc.edu/config -O config.sh ; sudo sh -e config.sh
```

## xfce

usage:

```
# (optional) run wget for external debs
ext_debs/wget.sh
# configure yourhost (ip or hostname)
./run.sh yourhost
```

### xfce_to_windows to adapt a new installed debian 11 with xfce desktop to newcomers that are familiar to windows environment

On a brand new debian 11 installation as of 2021-9-13, the following [xfce_to_windows.sh](./xfce_to_windows.sh) changes xfce settings to behave to what a new user would expect (specially when she has familiarity with the windows environment)

the script that only changes xfce is located on `type/__pedrolab_xfce_desktop/files/xfce_config.sh`

The xfce_to_windows is prepared to be executed just after the installation, other conditions were not considered

screenshot before applying the process:

![](xfce_before.png)

secreenshot after applying the process:

![](xfce_after.png)

#### Methodology

most of the changes where discovered applying [git](https://git-scm.com/) tracking of the `~/.config/xfce4` directory while performing the actions graphically, scripting/automating the changes and then testing them through an xfce4 environment that was reset to defaults

```
cd ~/.config/xfce4
git init
git add . 'init'
# do some change on panel
# after that, sometimes you have to reload the panel so it writes the changes to the files
xfce4-panel -r
git diff
git status
```

#### Reset to default xfce4

logout user session

go to a terminal such as Ctrl+Alt+F1, login as root (directly or through sudo) and do:

```
rm -rf /home/youruserhere/.config/xfce4
rm -rf /home/youruserhere/.config/dconf
# this is sometimes useful -> src https://forum.xfce.org/viewtopic.php?id=5806
rm -rf ~/.cache/sessions
pkill xfconfd
```

come back to graphics with Ctrl+Alt+F7 and log in again
