[TOC]

## Cómo instalar una aplicación en debian

En debian, idealmente casi todo te lo tendrías que instalar con `apt`. Esto ayuda a que tu sistema sea seguro porque son programas verificados por la comunidad de debian

Suponiendo que quiero instalar `miprograma`, entonces harías:

```
sudo apt install miprograma
```

Si no sabes cómo se llama, abre una terminal y ejecuta `sudo synaptic`, entonces prueba de buscar cómo se llama el paquete que quieres instalar, puedes probar con su nombre o con la función que desempeña, prueba en castellano o inglés

Si el paquete no hay forma de encontrarlo entramos en una zona de riesgo, esto es, hay que confiar en alguien externo para instalar el programa; y dicho programa no habría sido avalado por la comunidad de debian

Sin embargo, hay veces que no hay más remedio, entonces, el mejor método es ir a la web del creador del programa y evitar los intermediarios

Normalmente hay dos métodos de instalación de programas:

- El ejecutable van empaquetados como `.deb`, si estás descargando un programa de internet
- Te dicen de configurar un repositorio debian (de momento no lo explico porque cada programa suele explicar cómo hacerlo en caso de estar disponible este método)

Es ideal configurar repositorio debian porque cuando se actualice el sistema, se actualizará dicho programa; en caso de no disponer del repositorio debian quiere decir que manualmente tendremos que ir aplicando las actualizaciones manualmente: descargar un nuevo .deb e instalarlo

## Cómo descomprimir un fichero .zip

Desde el explorador de archivos (thunar), botón derecho sobre el fichero zip y click a "Extraer Aquí"

Para opciones más avanzadas, doble click sobre el zip y se nos abrirá xarchiver que nos ofrecerá diferentes métodos para extraer los ficheros. A través de botón derecho extraer, en el menú acción extraer, o los botones con iconos del menú

## Cómo instalar un fichero .deb

Doble click sobre el fichero .deb, debería abrirse una aplicación llamada gdebi con una ventana gráfica para instalarlo

En caso de que no lo pueda instalar, puede ser que necesite dependencias adicionales, probar entonces en una terminal con el comando:

```
sudo apt install -f
```

Si no hay manera de instalarlo probablemente es porque este programa .deb no está pensado para nuestra versión de debian

## Cómo actualizar el sistema operativo debian

Entra en terminal y ejecuta:

```
sudo synaptic
```

Una vez dentro da click a los tres botones grandes:

1. Recargar
2. Marcar todas las actualizaciones
3. Aplicar

En función de cómo de grande sea la actualización puede tardar un rato

Para actualizar desde terminal

```
sudo apt update
sudo apt upgrade
```

Si la actualización quiere borrar paquetes no continuar, mejor cancelar y probar con `aptitude`

```
sudo apt install aptitude
sudo aptitude upgrade
```

Si sigue yendo mal probar con

```
sudo apt dist-upgrade
```

### Ficheros de configuración modificados

Las actualizaciones llevan consigo mejoras en los programas que pueden cambiar la configuración para su uso

En caso de que debian sugiera un cambio de un fichero de configuración, saldrá un diálogo tal como:

```
Fichero de configuración `/etc/miconfig.cfg'
 ==> Modificado (por usted o por un script) desde la instalación.
 ==> El distribuidor del paquete ha publicado una versión actualizada.
   ¿Qué quisiera hacer al respecto?  Sus opciones son:
    Y o I  : instalar la versión del desarrollador del paquete
    N o O  : conservar la versión que tiene instalada actualmente
      D    : mostrar las diferencias entre versiones
      Z    : ejecutar un intérprete de órdenes para examinar la situación
 La acción por omisión es conservar la versión actual.
*** miconfig.cfg (Y/I/N/O/D/Z) [por omisión=N] ?
```

Y el usuario tendrá que decidir qué acción tomar, en caso de duda, apretando simplemente Enter aplicaría la N de No y sería la opción más conservadora, la de preservar el fichero de configuración

Más información (avanzado): https://raphaelhertzog.com/2010/09/21/debian-conffile-configuration-file-managed-by-dpkg/

### En caso de interrupción durante el proceso de actualización

Las actualizaciones pueden llegar a tomar mucho tiempo, y por el camino, el ordenador se puede apagar, o puede dejar de responder. El resultado es que cuando se vuelva a inicializar dejará una actualización a medias

En el siguiente intento por intentar acabar la actualización podemos recibir un mensaje tal como:

> Se interrumpió la ejecución de dpkg, debe ejecutar manualmente «sudo dpkg --configure -a» para corregir el problema

En tal caso, ejecutar en terminal el comando sugerido

```
sudo dpkg --configure -a
```

### Problema NO_PUBKEY

Durante `sudo apt update` podría aparecer el siguiente mensaje de error de que hay un problema con la llave pública `A0A0A0A0A0A0A0A0` del repositorio example.com:

> Las firmas siguientes no se pudieron verificar porque su clave pública no está disponible: NO_PUBKEY A0A0A0A0A0A0A0A0
> W: Fallo al obtener http://example.com/dists/stable/InRelease

entonces hacer algo tal como:

`sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys A0A0A0A0A0A0A0A0`

para obtener la llave pública que nos falta

al hacer otra vez `sudo apt update` no debería aparecer más dicho `NO_PUBKEY` para ese repositorio
