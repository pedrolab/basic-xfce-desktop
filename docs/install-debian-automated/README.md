warning: this guide is unfinished

[TOC]

## Prepare USB

Use an external media device to install debian. [Ventoy](https://www.ventoy.net/en/index.html) is the recommended way. [Check out this guide to install it](https://gitlab.com/guifi-exo/wiki/-/blob/master/howto/ventoy.md)

Use image [Debian 11.6 DVD1 with non-free drivers](https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/amd64/iso-dvd/firmware-11.6.0-amd64-DVD-1.iso) to ensure that you will have propietary drivers that will let you to continue during the installation. If link is broken it's because a new debian stable version appeared, temporary [find it on this link](https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/) meanwhile we update the documentation

## Boot installer and select appropriate install method

Once the debian installer has started, select *Advanced Options*:

![image](./boot_1_select-advanced.png)

next menu item is *Automated install*:

![image](./boot_2_select-automated-install.png)

or with the keyboard: press the following keys: A, Enter, A, Enter

## Install process

after some time it will ask to connect to internet, try to connect it through cable

TODO screenshot cable connection

wifi connection is not going to work, I don't know why

then, you will find some dialog asking for the URL, put the following (or use a url shortener):

https://dsg.ac.upc.edu/xfce-es

![image](./debconf-preconfiguration-file.png)


if for some reason you hit enter and you miss that dialog, or it fails and you want to re-run it, press *Go Back* (press up key until you reach it) and select it again in the installer menu:

![image](./installer-menu.png)

if you have more than one disk, it will show the following dialog, select *Guided - use entire disk* and press enter, and then select in what disk you want to install debian (it will format it automatically)

![image](./more-than-one-disk-situation.png)

*warning: primary disk is detected by grub-installer, on some situations where USB stick is detected as the first disk, grub will be installed incorrectly*

## Annex: wifi connection error

TODO screenshots to get there

## Troubleshooting

Press Alt + left and right to switch from the GUI installer, and a log that shows more information about what's going on with the installer
