#!/bin/sh -e

# place the downloads near the script
cd "$(dirname "${0}")"

for pkg in $(ls -1 *.deb); do
  echo "$(dpkg-deb --show --showformat='${binary:Package}_${Version}_${Architecture}.deb\n' "${pkg}")"
done

