#!/bin/sh -e

# this script downloads the .deb that are not tried to download with the
#   _download function

# src https://stackoverflow.com/questions/4944295/skip-download-if-files-already-exist-in-wget
_process() {
  name="${1}.deb"
  url="${2}"
  curl -L "${url}" -z "${name}" -o "${name}" || return 0
  # move outside removal process
  postinst_path="debs/${pkgname}/postinst.sh"
  if [ -f "${postinst_path}" ]; then
    ${postinst_path}
  fi
  mv "${name}" to_save
}

# place the downloads near the script
cd "$(dirname "${0}")"

# strategy to ensure that the debs not processed are deleted
mkdir -p to_save
for pkgname in $(ls -1 "debs"); do
  url="$(cat "debs/${pkgname}/url")"
  _process "${pkgname}" "${url}"
done
# remove debs not saved
rm -f *.deb
mv to_save/*.deb .
rmdir to_save
# remove next line in 2022-8-1
rmdir to_remove 2>/dev/null || true
