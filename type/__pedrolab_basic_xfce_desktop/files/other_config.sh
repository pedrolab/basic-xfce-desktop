#!/bin/sh -e

# put autologin for 1000 gid user
targetuser="$(cat /etc/passwd | grep 1000 | cut -d':' -f1)"

sed -i'' -E "s/^[#]?[[:space:]]*autologin-user=.*/autologin-user=${targetuser}/g" /etc/lightdm/lightdm.conf

# list users
sed -i'' -E 's/^[#]?[[:space:]]*greeter-hide-users=.*/greeter-hide-users=false/g' /etc/lightdm/lightdm.conf

# passwordless sudo
sed -i'' -E 's/^%sudo.*ALL=\(ALL:ALL\) ALL/%sudo	ALL=(ALL:ALL)	NOPASSWD:ALL/g' /etc/sudoers

# make gdebi work, we use sudo approach to avoid password prompt
#  for password prompt, running with terminal=true looks like the best approach
#    src https://unix.stackexchange.com/questions/405520/gdebi-keeps-crashing-when-install-is-clicked
#   TODO this is a bad solution :(
sed -i'' -E 's/Exec=gdebi-gtk %f/Exec=sudo gdebi-gtk %f/g' /usr/share/applications/gdebi.desktop

# run only the first time if this was never set
#  chromium-based browsers want a default keyring to avoid annoying message
#  asking for a password. this sets a blank password for password keyring
#    TODO we should show people how to use seahorse and manage their secrets more appropiately
#    TODO better integration of gnome-keyring with xfce
# src https://unix.stackexchange.com/questions/589300/access-seahorse-passwords-from-terminal
# src https://wiki.archlinux.org/title/GNOME/Keyring
if [ ! -f /home/${targetuser}/.local/share/keyrings/default ]; then
  mkdir -p /home/${targetuser}/.local/share/keyrings/
  chown -R ${targetuser}: /home/${targetuser}/.local/share/keyrings/
  chmod 0700 /home/${targetuser}/.local/share/keyrings/
  # make a default keyring
  echo "autogenerado" > /home/${targetuser}/.local/share/keyrings/default
  cat > /home/${targetuser}/.local/share/keyrings/autogenerado.keyring <<EOF
[keyring]
display-name=autogenerado
ctime=1635827452
mtime=0
lock-on-idle=false
lock-after=false
EOF
  chown ${targetuser}: /home/${targetuser}/.local/share/keyrings/default
  chmod 0644           /home/${targetuser}/.local/share/keyrings/default
  chown ${targetuser}: /home/${targetuser}/.local/share/keyrings/autogenerado.keyring
  chmod 0600           /home/${targetuser}/.local/share/keyrings/autogenerado.keyring
fi

# replace root's .bashrc with the one from skel in order to have bash_completion
grep -qx "bash_completion" /etc/skel/.bashrc || cp -a /etc/skel/.bashrc /root/

