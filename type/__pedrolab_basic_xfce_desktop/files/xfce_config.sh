#!/bin/sh -e
# SPDX-License-Identifier: Apache-2.0

# vars to launch X11 from a script in ssh \\
#   -> src https://forum.manjaro.org/t/permissions-to-run-xfconf-query-from-systemd-scripts/53513/2
# UID is not in sh, but bash \\
#   -> src https://stackoverflow.com/questions/39445968/bash-getting-uid-on-shell-script-does-not-work
export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$(id -u)/bus"
export DISPLAY=:0
export XAUTHORITY=/home/$USER/.Xauthority

# verify is not run as root
#   https://serverfault.com/questions/568627/can-a-program-tell-it-is-being-run-under-sudo/568628#568628
if [ "$(id -u)" -eq 0 ]; then
  echo "ERROR: root detected, run the script as user to success"
  exit 1
fi

# assuming debian 11 with xfce as of 2021-9-13

get_plugin_path() {
    application="${1}"
    id="$(xfconf-query -c xfce4-panel -p /plugins -lv | grep "${application}" | awk '{ print $1; }')"
    echo "${id}"
}

# backup current config
cp -ar ~/.config/xfce4 ~/.config/xfce4-bak-$(date +'%Y%m%d_%H%M%S')
cp -ar ~/.config/dconf ~/.config/dconf-bak-$(date +'%Y%m%d_%H%M%S')

# remove/hide launcher panel -> src https://forum.xfce.org/viewtopic.php?id=12792
xfconf-query -c xfce4-panel -p /panels -t int -s 1 -a
# move primary panel to bottom
xfconf-query -c xfce4-panel -p /panels/panel-1/position -t string -s "p=8;x=0;y=0"
# manage plugins
#   ref1 https://stackoverflow.com/questions/51808766/how-to-add-xfce4-panel-plugin-to-the-specific-panel-via-terminal
#   ref2 https://forum.xfce.org/viewtopic.php?id=8619
# replace plugin applicationmenu with whiskermenu
appmenu_path="$(get_plugin_path applicationsmenu)"
if [ -n "${appmenu_path}" ]; then
  xfconf-query -c xfce4-panel -p "${appmenu_path}" -t string -s whiskermenu
fi

# whisker menu: view as icons
#   lockscreen not shown as this config is not really secure
#   logout menu tends to save session which is strange and difficult/impossible(?) to disable
cat > ~/.config/xfce4/panel/whiskermenu-1.rc <<EOF
button-title=Inicio
button-icon=debian-logo
show-button-title=true
show-button-icon=true
view-as-icons=false
show-command-lockscreen=false
show-command-restart=true
show-command-shutdown=true
show-command-suspend=true
show-command-hibernate=true
show-command-logout=false
EOF

# classic windows key shortcut to whisker menu
#   src https://www.pragmaticlinux.com/2021/03/install-and-configure-the-whisker-menu-as-your-xfce-start-menu/
xfconf-query --create -c xfce4-keyboard-shortcuts -p /commands/custom/Super_L -t string -s "xfce4-popup-whiskermenu"
xfce4-popup-whiskermenu

# quit the session applet
actions_path="$(get_plugin_path actions)"
if [ -n "${actions_path}" ]; then
  xfconf-query -c xfce4-panel -p "${actions_path}" -r
fi
# reduce number of workspaces from 4 to 1
xfconf-query -c xfwm4 -p /general/workspace_count -t int -s "1"
# quit workspace plugin
ws_path="$(get_plugin_path pager)"
if [ -n "${ws_path}" ]; then
  xfconf-query -c xfce4-panel -p "${ws_path}" -r
fi

# disable window grouping -> https://ubuntuincident.wordpress.com/2016/05/13/xfce-disable-window-grouping/
group_path="$(get_plugin_path grouping)"
xfconf-query -c xfce4-panel -p "${group_path}" -t int -s 0

# reduce potential screen-tearing problems -> src https://wiki.debian.org/Xfce#Preventing_screen-tearing
xfconf-query -c xfwm4 -p /general/use_compositing -t bool -s "false"

# never lock screen (I suspect this is a bug present in default config for debian11 and xfce desktop)
xfconf-query --create -c xfce4-power-manager -p /xfce4-power-manager/lock-screen-suspend-hibernate -t bool -s "false"
xfconf-query --create -c xfce4-power-manager -p /xfce4-power-manager/logind-handle-lid-switch -t bool -s "false"
# lid action (close lid on a laptop), from now: suspend
#   0: shutdown screen
#   1: suspend
#     strange screen lock when suspend (?) switching to shutdown screen
#   2: hibernate
#   3: lock screen
xfconf-query --create -c xfce4-power-manager -p /xfce4-power-manager/lid-action-on-battery -t uint -s "0"
xfconf-query --create -c xfce4-power-manager -p /xfce4-power-manager/lid-action-on-ac -t uint -s "0"
# power manager / security / automatically lock the session: never
#   this option is surprisingly managed through dconf
#   ref https://askubuntu.com/questions/22313/what-is-dconf-what-is-its-function-and-how-do-i-use-it
gsettings set apps.light-locker lock-after-screensaver 0

# make panel bigger
xfconf-query -c xfce4-panel -p /panels/panel-1/size -t int -s 30

# font size 12
#   extra https://askubuntu.com/questions/744616/xubuntu-14-04-system-appearance-fonts-not-effective
xfconf-query -c xsettings -p /Gtk/FontName -s "Sans 12"
xfconf-query -c xsettings -p /Gtk/MonospaceFontName -s "Monospace 12"
xfconf-query -c xfwm4 -p /general/title_font -s "Sans Bold 12"

# quit strange arrow symbol from xfce windows
xfconf-query -c xfwm4 -p /general/button_layout -s "O|HMC"

# avoid saving the session -> src https://wiki.archlinux.org/title/Xfce#Disable_saved_sessions
xfconf-query -c xfce4-session -p /general/SaveOnExit -s false

# file explorer (thunar) config
xfconf-query --create -c thunar -p /last-side-pane -t string -s "ThunarTreePane"
xfconf-query --create -c thunar -p /default-view -t string -s "ThunarDetailsView"
#  modify date style
xfconf-query --create -c thunar -p /misc-date-style -t string -s "THUNAR_DATE_STYLE_YYYYMMDD"

# automount removable devices
xfconf-query --create -c thunar-volman -p /automount-media/enabled -t bool -s "true"
xfconf-query --create -c thunar-volman -p /automount-drives/enabled -t bool -s "true"
xfconf-query --create -c thunar-volman -p /autobrowse/enabled -t bool -s "true"
xfconf-query --create -c thunar-volman -p /autoopen/enabled -t bool -s "true"

# tap to click should be done manually because it depends on hardware
#   ref1 https://superuser.com/questions/1274579/xfce-tap-to-click-debian9
#   ref2 https://docs.xfce.org/xfce/xfce4-settings/mouse
#   ref3 https://unix.stackexchange.com/questions/627607/command-to-toggle-on-off-touchpad-in-xfce
# if we don't have this touchpad, nothing happens, that's fine, then we can add all we know

touchpads=$( grep -v '#' <<EOF
SynPS2_Synaptics_TouchPad
AlpsPS2_ALPS_GlidePoint
Synaptics_TM2927-001
Synaptics_TM2749-001
Synaptics_TM3075-002
EOF
)
for t in ${touchpads}; do
  xfconf-query --create -c pointers -p /${t}/Properties/libinput_Tapping_Enabled -t int -s "1"
done

# gnome-keyring in a different way, check other_config.sh
#   avoid gnome-keyring prompt -> src https://medium.com/the-blog-of-ehsan-nazim/solution-to-enter-password-to-unlock-your-login-keyring-prompt-while-opening-chromium-google-839861f49727
#     alternative would be to remove gnome-keyring and so on -> src https://www.linuxquestions.org/questions/slackware-14/chrome-in-xfce-%27enter-password-for-keyring-default-to-unlock%27-4175608133/
#xfconf-query --create -c xfce4-session -p /compat/LaunchGNOME -t bool -s "true"

# choose default apps (mime) -> src https://wiki.archlinux.org/title/Xdg-utils
xdg-mime default mousepad.desktop text/plain

# restart panel
xfce4-panel -r
